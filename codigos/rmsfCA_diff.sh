#! /bin/bash

SISTEMAS="lami1 lami_t90 2HYK 2HYK_t90 2CL2 2CL2_t90"
SIMULACOES="simA simB simC"

for item in $SISTEMAS
do
    i=0
    temp=$(tempfile)
    tail -n +35 $0 > $temp

    for sim in $SIMULACOES
    do
        simulacao="pca_${item}_${sim}.rmsfCA.dat"
        sed -i "s/#${sim}#/${simulacao}/" $temp

        sims_array[$i]=$simulacao
        i=$(($i +1))
    done

    median="pca_${item}_median.rmsfCA.dat"
    output="${item}_rmsfCA_diff.agr"

    sed -i "s/#MEDIAN#/${median}/" $temp
    sed -i "s/#OUTPUT#/${output}/" $temp
    sed -i "s/^#//" $temp
   
    ./median.py ${sims_array[@]} > "pca_${item}_median.rmsfCA.dat"
    xmgrace -batch $temp -nosafe -hardcopy

done




##########################XMGRACE_SCRIPT########################
#read "#MEDIAN#"
#read "#simA#"
#read "#simB#"
#read "#simC#"

#autoscale

#xaxis label "RESIDUO"
#yaxis label "rmsfCA"

#copy s1 to s4
#s4.y = s4.y - s0.y
#copy s2 to s5
#s5.y = s5.y - s0.y
#copy s3 to s6
#s6.y = s6.y - s0.y

#G0.S0 HIDDEN TRUE
#G0.S1 HIDDEN TRUE
#G0.S2 HIDDEN TRUE
#G0.S3 HIDDEN TRUE

#S4  legend "simA"
#S5  legend "simB"
#S6  legend "simC"

#saveall "#OUTPUT#"
