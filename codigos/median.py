#! /usr/bin/python3

import sys
import re
import math

def load_files(system_arguments):
    data = list()
    files = system_arguments[1:]

    file_list = [open(file, 'r') for file in files]

    data_regex = re.compile('^\s*(\d+)\s*([0-9.]+)')

    for i, file in enumerate(file_list):
        data.append(list())

        for line in file:
            data_match = data_regex.match(line)
            if data_match:
                data[i].append(data_match.group(1,2))

    return data

def extract_median(item):
    median_int = math.ceil(len(item)/2.0) - 1
    return item[median_int]

def process_data(data):
    num_lists = len(data)
    output_values = list()
    
    if (num_lists > 0):
        num_entries = len(data[0])
    else:
        sys.exit

    for x in range(num_entries):
        item = [(int(values[x][0]), float(values[x][1])) for values in data]
        sorted_item = sorted(item, key=lambda valor: valor[1])
        output_values.append(extract_median(sorted_item))

    return output_values

def print_output(values):

    format = "%6i        %.4f"
    for item in values:
        print(format % item)


print_output(process_data(load_files(sys.argv)))
